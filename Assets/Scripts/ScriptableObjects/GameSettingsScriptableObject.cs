using System;
using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Scriptable Objects/Game Settings")]
public class GameSettingsScriptableObject : ScriptableObject
{
    public float MasterVolume;
    public float MusicVolume;
    public float EffectsVolume;
    public float UIVolume;

    public int ResolutionWidth;
    public int ResolutionHeight;
    public bool Fullscreen;

    public int GraphicsPreset;

    public void Load(string filePath)
    {
        Settings settings;

        if (File.Exists(filePath))
        {
            string json = File.ReadAllText(filePath);

            settings = JsonUtility.FromJson<Settings>(json);
            
            UpdateData(settings);
        }
        else
        {
            settings = new Settings();
            
            UpdateData(settings);
            
            Save(filePath);
        }
    }

    public void Save(string filePath)
    {
        string json = JsonUtility.ToJson(this, true);
        
        File.WriteAllText(filePath, json);
    }

    private void UpdateData(Settings settings)
    {
        MasterVolume = settings.MasterVolume;
        MusicVolume = settings.MusicVolume;
        EffectsVolume = settings.EffectsVolume;
        UIVolume = settings.UIVolume;
                
        ResolutionHeight = settings.ResolutionHeight;
        ResolutionWidth = settings.ResolutionWidth;
        Fullscreen = settings.Fullscreen;
        
        GraphicsPreset = settings.GraphicsPreset;
    }
}

[Serializable]
public class Settings
{
    public float MasterVolume = 1.0f;
    public float MusicVolume = 1.0f;
    public float EffectsVolume = 1.0f;
    public float UIVolume = 1.0f;
    
    public int ResolutionWidth;
    public int ResolutionHeight;
    public bool Fullscreen = true;

    public int GraphicsPreset = 2;

    public Settings()
    {
        ResolutionWidth = Screen.currentResolution.width;
        ResolutionHeight = Screen.currentResolution.height;
    }
}
