public enum SceneIndexes
{
    SplashScreen = 0,
    MainMenu = 1,
    GlobalManagers = 2,
    LoadingScreen = 3,
    HarryLevel = 4,
    KerriLevel = 5,
}