using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class DeathTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var stats = other.GetComponent<PlayerStats>();
        
        if (stats)
        {
            stats.TakeDamage();
        }
    }
}
