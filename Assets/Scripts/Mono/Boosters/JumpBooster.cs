using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class JumpBooster : MonoBehaviour, IPoundable
{
    private AudioSource _audioSource;
    
    [SerializeField] private AudioClip jumpSound;
    [SerializeField] private float jumpHeight = 5f;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void OnSmash(GameObject player)
    {
        _audioSource.PlayOneShot(jumpSound);
        
        var controller = player.GetComponent<WalkablePlayerController>();
        
        if (!controller)
            return;

        controller.VerticalVelocity = transform.up * Mathf.Sqrt(jumpHeight * -2f * Physics.gravity.y);
    }
}
