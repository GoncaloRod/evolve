using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EndLevelTrigger : MonoBehaviour
{
    private GameManager _gameManager;
    
    private bool _wasTriggered = false;

    private void Start()
    {
        _gameManager = GameManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_wasTriggered)
            return;
        
        bool isPlayer = other.GetComponent<PlayerController>() != null;
       
        if (isPlayer)
        {
            _gameManager.OnLevelEndReached.Invoke();
        }
    }
}
