using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePickup : Pickup
{
    protected override void OnPickup(PlayerStats stats)
    {
        stats.AddLife();
    }
}
