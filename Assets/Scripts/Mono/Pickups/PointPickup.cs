using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointPickup : Pickup
{
    protected override void OnPickup(PlayerStats stats)
    {
        stats.AddBone();
    }
}
