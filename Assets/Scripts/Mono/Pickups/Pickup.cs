using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public abstract class Pickup : MonoBehaviour
{
    private AudioSource _audioSource;

    private Collider _collider;
    private MeshRenderer _meshRenderer;

    [SerializeField] private AudioClip pickupSound;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        _collider = GetComponent<Collider>();
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (pickupSound != null)
        {
            _collider.enabled = false;
            _meshRenderer.enabled = false;
        
            _audioSource.PlayOneShot(pickupSound);
        }
        
        // TODO: Maybe some particles
        // TODO: Animation? Who knows

        PlayerStats stats = other.GetComponent<PlayerStats>();
        
        if (stats == null)
            return;
        
        OnPickup(stats);
        
        Destroy(gameObject, pickupSound != null? pickupSound.length : 0f);
    }

    protected abstract void OnPickup(PlayerStats stats);
}
