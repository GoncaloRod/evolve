using System.Runtime.InteropServices;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    public static CheckpointManager Instance;

    private GameObject _player;

    [SerializeField] private Transform currentCheckpoint;

    public Transform CurrentCheckpoint
    {
        get => currentCheckpoint;
        set => UpdateCheckpoint(value);
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        _player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Start()
    {
        Transform playerTransform = _player.transform;

        currentCheckpoint.position = playerTransform.position;
        currentCheckpoint.rotation = playerTransform.rotation;

        GameManager.Instance.OnPlayerDie.AddListener(ReturnToCheckpoint);
    }

    private void UpdateCheckpoint(Transform newCkeckpoint)
    {
        currentCheckpoint.position = newCkeckpoint.position;
        currentCheckpoint.rotation = newCkeckpoint.rotation;
    }

    private void ReturnToCheckpoint()
    {
        if (_player.GetComponent<WalkablePlayerController>())
        {
            var controller = _player.GetComponent<CharacterController>();
            controller.enabled = false;
            _player.transform.position = currentCheckpoint.position;
            controller.enabled = true;
        }
        else if (_player.GetComponent<AquaticPlayerController>())
        {
            _player.transform.position = currentCheckpoint.transform.position;
        }
    }
}
