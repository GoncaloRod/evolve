using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    private SettingsManager _settingsManager;
    
    [SerializeField] private AudioMixer mixer;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        Debug.Assert(mixer != null, $"Missing audio mixer reference in {name}");
        
        _settingsManager = SettingsManager.Instance;
        
        _settingsManager.OnSettingsChange.AddListener(UpdateAudioVolumes);
    }

    private void UpdateAudioVolumes()
    {
        GameSettingsScriptableObject settings = _settingsManager.Settings;

        mixer.SetFloat("MasterVolume", -20 * (1 - settings.MasterVolume));
        mixer.SetFloat("MusicVolume", -20 * (1 - settings.MusicVolume));
        mixer.SetFloat("EffectsVolume", -20 * (1 - settings.EffectsVolume));
        mixer.SetFloat("UIVolume", -20 * (1 - settings.UIVolume));
    }
}
