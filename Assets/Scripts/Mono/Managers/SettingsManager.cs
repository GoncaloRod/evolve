using UnityEngine;
using UnityEngine.Events;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager Instance;

    private const string SettingFileName = "settings.json";

    [SerializeField] private GameSettingsScriptableObject _settings;

    public UnityEvent OnSettingsChange;
    
    [HideInInspector] public GameSettingsScriptableObject Settings => _settings;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        OnSettingsChange = new UnityEvent();
        
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        _settings.Load(Application.persistentDataPath + "/" + SettingFileName);
        
        ApplySettings();
    }

    private void SaveSettings()
    {
        _settings.Save(Application.persistentDataPath + "/" + SettingFileName);
    }

    public void ApplySettings()
    {
        // Video
        Screen.SetResolution(
            _settings.ResolutionWidth, _settings.ResolutionHeight, _settings.Fullscreen
        );
        
        // Graphics
        QualitySettings.SetQualityLevel(_settings.GraphicsPreset);
        
        OnSettingsChange.Invoke();
        
        SaveSettings();
    }
}
