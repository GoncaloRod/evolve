using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public UnityEvent OnPlayerDie { get; private set; }

    public UnityEvent OnGameOver { get; private set; }
    
    public UnityEvent OnGamePause { get; private set; }
    
    public UnityEvent OnGameResume { get; private set; }
    
    public UnityEvent OnLevelEndReached { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        OnPlayerDie = new UnityEvent();
        OnGameOver = new UnityEvent();
        OnGamePause = new UnityEvent();
        OnGameResume = new UnityEvent();
        OnLevelEndReached = new UnityEvent();
    }
}
