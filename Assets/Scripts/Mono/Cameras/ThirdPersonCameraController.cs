using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineFreeLook))]
public class ThirdPersonCameraController : MonoBehaviour
{
    private InputActions _actions;
    
    private CinemachineFreeLook _camera;

    [SerializeField] private float xSensitivity = 1f;
    [SerializeField] private float ySensitivity = 1f;

    private void Awake()
    {
        _actions = new InputActions();
        
        _camera = GetComponent<CinemachineFreeLook>();
    }

    private void Update()
    {
        Vector2 input = _actions.Player.CameraRotation.ReadValue<Vector2>();
        
        _camera.m_YAxis.Value = Mathf.Clamp(_camera.m_YAxis.Value + input.y * ySensitivity * Time.deltaTime, 0f, 1f);

        _camera.m_XAxis.Value += input.x * xSensitivity * Time.deltaTime;

        if (_camera.m_XAxis.Value > 180f)
            _camera.m_XAxis.Value -= 180f;
        else if (_camera.m_XAxis.Value < -180f)
            _camera.m_XAxis.Value += 180f;
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();
    }
}
