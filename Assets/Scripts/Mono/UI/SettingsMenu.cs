using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    private SettingsManager _settingsManager;
    
    private (int Width, int Height)[] _resolutions;
    private string[] _qualities;
    
    private int _currentResolutionIndex = -1;
    private int _currentGraphicsPresetIndex = -1;

    [Header("Audio")]
    [SerializeField] private Slider masterVolumeSlider;
    [SerializeField] private Slider musicVolumeSlider;
    [SerializeField] private Slider effectsVolumeSlider;
    [SerializeField] private Slider uiVolumeSlider;
    
    [Header("Video")]
    [SerializeField] private TMP_Dropdown resolutionDropdown;
    [SerializeField] private Toggle fullscreenCheckbox;
    
    [Header("Graphics")]
    [SerializeField] private TMP_Dropdown graphicsPresetDropdown;

    [Space]
    [SerializeField] private Button applyButton;
    
    private void Start()
    {
        _settingsManager = SettingsManager.Instance;
        
        var currentResolution = Screen.currentResolution;
        
        GetSupportedResolutions(currentResolution);
        
        _qualities = QualitySettings.names;
        _currentGraphicsPresetIndex = QualitySettings.GetQualityLevel();

        PopulateDropdowns();

        masterVolumeSlider.value = _settingsManager.Settings.MasterVolume;
        musicVolumeSlider.value = _settingsManager.Settings.MusicVolume;
        effectsVolumeSlider.value = _settingsManager.Settings.EffectsVolume;
        uiVolumeSlider.value = _settingsManager.Settings.UIVolume;
        
        masterVolumeSlider.onValueChanged.AddListener(OnMasterVolumeSliderChange);
        musicVolumeSlider.onValueChanged.AddListener(OnMusicVolumeSliderChange);
        effectsVolumeSlider.onValueChanged.AddListener(OnEffectsVolumeSliderChange);
        uiVolumeSlider.onValueChanged.AddListener(OnUIVolumeSliderChange);
        
        resolutionDropdown.onValueChanged.AddListener(OnResolutionDropdownChange);
        fullscreenCheckbox.onValueChanged.AddListener(OnFullscreenCheckboxChange);
        
        graphicsPresetDropdown.onValueChanged.AddListener(OnGraphicsPresetDropdownChange);
        
        applyButton.onClick.AddListener(ApplySettings);
    }

    private void GetSupportedResolutions(Resolution currentResolution)
    {
        var screenResolutions = Screen.resolutions;
        
        var tempResolutions = new List<(int Width, int Height)>();
        var tempRefreshRates = new List<int>();
        
        foreach (var screenRes in screenResolutions)
        {
            if (!tempResolutions.Any(res => res.Width == screenRes.width && res.Height == screenRes.height))
                tempResolutions.Add((screenRes.width, screenRes.height));
            
            if (!tempRefreshRates.Contains(screenRes.refreshRate))
                tempRefreshRates.Add(screenRes.refreshRate);
        }

        _resolutions = tempResolutions.ToArray();
        
        for (int i = 0; i < _resolutions.Length; i++)
            if (_resolutions[i].Width == currentResolution.width && _resolutions[i].Height == currentResolution.height)
                _currentResolutionIndex = i;
    }

    private void PopulateDropdowns()
    {
        // Resolutions
        resolutionDropdown.options.Clear();
        
        foreach (var resolution in _resolutions)
            resolutionDropdown.options.Add(new TMP_Dropdown.OptionData($"{resolution.Width} x {resolution.Height}"));
        
        if (_currentResolutionIndex >= 0)
            resolutionDropdown.value = _currentResolutionIndex;
        
        // Graphics preset
        graphicsPresetDropdown.options.Clear();
        
        foreach (string quality in _qualities)
            graphicsPresetDropdown.options.Add(new TMP_Dropdown.OptionData(quality));

        graphicsPresetDropdown.value = _currentGraphicsPresetIndex;
    }

    private void OnMasterVolumeSliderChange(float value)
    {
        _settingsManager.Settings.MasterVolume = value;
    }

    private void OnMusicVolumeSliderChange(float value)
    {
        _settingsManager.Settings.MusicVolume = value;
    }
    
    private void OnEffectsVolumeSliderChange(float value)
    {
        _settingsManager.Settings.EffectsVolume = value;
    }
    
    private void OnUIVolumeSliderChange(float value)
    {
        _settingsManager.Settings.UIVolume = value;
    }

    private void OnResolutionDropdownChange(int index)
    {
        _currentResolutionIndex = index;

        _settingsManager.Settings.ResolutionWidth = _resolutions[index].Width;
        _settingsManager.Settings.ResolutionHeight = _resolutions[index].Height;
    }

    private void OnFullscreenCheckboxChange(bool isChecked)
    {
        _settingsManager.Settings.Fullscreen = isChecked;
    }

    private void OnGraphicsPresetDropdownChange(int index)
    {
        _currentGraphicsPresetIndex = index;

        _settingsManager.Settings.GraphicsPreset = index;
    }

    private void ApplySettings()
    {
        _settingsManager.ApplySettings();
    }
}
