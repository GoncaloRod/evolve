using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCameraRotation : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform; 
    [SerializeField] private float speed = 1f;

    private void Awake()
    {
        Debug.Assert(cameraTransform != null, "Missing camera reference");
    }

    private void Update()
    {
        Vector3 rotation = transform.rotation.eulerAngles + Vector3.up * (speed * Time.deltaTime);

        transform.rotation = Quaternion.Euler(rotation);
        cameraTransform.transform.LookAt(transform);
    }
}
