using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    private void Start()
    {
        SceneManager.LoadScene((int)SceneIndexes.GlobalManagers, LoadSceneMode.Additive);
    }

    public void Finish()
    {
        SceneManager.LoadScene((int)SceneIndexes.MainMenu);
    }
}
