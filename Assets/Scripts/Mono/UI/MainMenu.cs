using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject settingsPanel;
    
    public void OnPlayButtonPressed()
    {
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = (int)SceneIndexes.HarryLevel;
        
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }

    public void OnSettingsButtonPressed()
    {
        settingsPanel.SetActive(!settingsPanel.activeSelf);
    }

    public void OnExitButtonPressed()
    {
        Application.Quit();
    }
}
