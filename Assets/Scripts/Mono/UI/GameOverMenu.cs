using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private GameObject gameOverMenuPanel;

    private void Awake()
    {
        gameOverMenuPanel.SetActive(false);
    }

    private void Start()
    {
        GameManager.Instance.OnGameOver.AddListener(() =>
        {
            gameOverMenuPanel.SetActive(true);
        });
    }

    public void OnMainMenuButtonPressed()
    {
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = (int)SceneIndexes.MainMenu;
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }

    public void OnRetryLevelButtonPressed()
    {
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }
}
