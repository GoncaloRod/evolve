﻿using System;
using UnityEngine;
using TMPro;

    public class UIStats : MonoBehaviour
    {
        private PlayerStats _stats;
        [SerializeField] private TMP_Text livesDisplay;
        [SerializeField] private TMP_Text bonesDisplay;
        private void Awake()
        {
            GameObject player = GameObject.FindWithTag("Player");
            _stats = player.GetComponent<PlayerStats>();
        }
        private void Update()
        {
            if (_stats.Life>=0)
                livesDisplay.text = _stats.Life.ToString();
            else
                livesDisplay.text = String.Empty;
            
            
            bonesDisplay.text = _stats.Bones.ToString();
        }
    }
