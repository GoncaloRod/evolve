using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private InputActions _actions;

    private bool _paused = false;

    [SerializeField] private GameObject pauseMenuPanel;
    [SerializeField] private GameObject settingsMenuPanel;

    private void Awake()
    {
        _actions = new InputActions();

        _actions.UI.Pause.performed += _ => TogglePause();
        
        pauseMenuPanel.SetActive(false);
    }

    private void TogglePause()
    {
        if (settingsMenuPanel.activeSelf && !pauseMenuPanel.activeSelf)
        {
            OnCloseSettingsButtonPressed();
        }
        else
        {
            _paused = !_paused;
                    
            Time.timeScale = _paused ? 0f : 1f;
            
            pauseMenuPanel.SetActive(_paused);
    
            if (_paused)
                GameManager.Instance.OnGamePause.Invoke();
            else
                GameManager.Instance.OnGameResume.Invoke();
        }
    }

    public void OnResumeButtonPressed()
    {
        TogglePause();
    }

    public void OnSettingsButtonPressed()
    {
        pauseMenuPanel.SetActive(false);
        settingsMenuPanel.SetActive(true);
    }

    public void OnCloseSettingsButtonPressed()
    {
        pauseMenuPanel.SetActive(true);
        settingsMenuPanel.SetActive(false);
    }

    public void OnExitButtonPressed()
    {
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = (int)SceneIndexes.MainMenu;

        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();
    }
}
