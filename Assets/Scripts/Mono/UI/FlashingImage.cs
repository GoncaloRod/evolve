using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class FlashingImage : MonoBehaviour
{
    private Image _image;
    
    [SerializeField] private float speed = 1f;
    
    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    private void Update()
    {
        Color color = new Color(1.0f, 1.0f, 1.0f, Mathf.Sin(Time.time / speed));

        _image.color = color;
    }
}
