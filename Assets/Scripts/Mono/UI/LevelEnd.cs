using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour
{
    [SerializeField] private GameObject levelEndPanel;
    [SerializeField] private GameObject nextLevelButton;
    
    [Space]
    [SerializeField] private bool hasNextLevel = false;
    [SerializeField] private SceneIndexes nextLevel;

    private void Awake()
    {
        levelEndPanel.SetActive(false);

        nextLevelButton.SetActive(hasNextLevel);
    }

    private void Start()
    {
        GameManager.Instance.OnLevelEndReached.AddListener(() =>
        {
            Time.timeScale = 0f;
            levelEndPanel.SetActive(true);
        });
    }

    public void OnMainMenuButtonPressed()
    {
        Time.timeScale = 1f;
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = (int)SceneIndexes.MainMenu;
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }

    public void OnRetryLevelButtonPressed()
    {
        Time.timeScale = 1f;
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }

    public void OnNextLevelButtonPressed()
    {
        Time.timeScale = 1f;
        LoadingScreen.LoadSceneBy = LoadSceneBy.Index;
        LoadingScreen.sceneToLoadIndex = (int)nextLevel;
        SceneManager.LoadScene((int)SceneIndexes.LoadingScreen);
    }
}
