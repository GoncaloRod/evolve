using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum LoadSceneBy
{
    Index,
    Name
}

public class LoadingScreen : MonoBehaviour
{
    public static LoadSceneBy LoadSceneBy;
    public static int sceneToLoadIndex;
    public static string sceneToLoadName;

    [SerializeField] private Slider progressBar;

    private void Awake()
    {
        StartCoroutine(LoadScene());
    }

    private IEnumerator LoadScene()
    {
        AsyncOperation loading = LoadSceneBy switch
        {
            LoadSceneBy.Index => SceneManager.LoadSceneAsync(sceneToLoadIndex),
            LoadSceneBy.Name => SceneManager.LoadSceneAsync(sceneToLoadName),
            _ => throw new ArgumentOutOfRangeException()
        };

        loading.allowSceneActivation = false;
        loading.completed += OnLoadingComplete;

        while (!loading.isDone)
        {
            UpdateProgressBar(loading.progress);
            
            yield return null;
        }
    }

    private void UpdateProgressBar(float progress)
    {
        progressBar.value = progress;
    }

    private void OnLoadingComplete(AsyncOperation loading)
    {
        Debug.Log("Loading complete!");
        loading.allowSceneActivation = true;
    }
}
