using System;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    private InputActions _inputActions;

    private PlayerController _controller;

    private Vector2 _movementInput;
    private bool _jumpInput;

    private void Awake()
    {
        _inputActions = new InputActions();
        
        _controller = GetComponent<PlayerController>();

        _inputActions.Player.Jump.performed += _ => _controller.Jump();
        _inputActions.Player.GroundPound.performed += _ => _controller.Pound();
    }

    private void Start()
    {
        // NOTE: This could be done a little better, not great, not terrible
        GameManager.Instance.OnGamePause.AddListener(() =>
        {
            enabled = false;
        });
        
        GameManager.Instance.OnGameResume.AddListener(() =>
        {
            enabled = true;
        });
        
        GameManager.Instance.OnGameOver.AddListener(() =>
        {
            enabled = false;
        });
        
        GameManager.Instance.OnLevelEndReached.AddListener(() =>
        {
            enabled = false;
        });
    }

    private void Update()
    {
        _movementInput = _inputActions.Player.Movement.ReadValue<Vector2>();
        _movementInput.Normalize();

        _controller.MovementInput = _movementInput;
    }

    private void OnEnable()
    {
        _inputActions.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Disable();
    }
}
