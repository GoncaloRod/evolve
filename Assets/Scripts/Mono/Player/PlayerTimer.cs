﻿using System;
using System.Collections;
using UnityEngine;

using TMPro;

public class PlayerTimer : MonoBehaviour
{

    private float _elapsedTime;
    private TimeSpan _timePlaying;
    private bool _timerRunning;
    
    public static PlayerTimer Instance;
    public TMP_Text TimeCounter;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GameManager.Instance.OnGameOver.AddListener(StopTimer);
        
        TimeCounter.text = "00:00";
        _timerRunning = false;
        BeginTimer();
    }

    public void BeginTimer()
    {
        _timerRunning = true;
        _elapsedTime = 0f;

        StartCoroutine(UpdateTimer());
    }

    public void StopTimer()
    {
        _timerRunning = false;
    }

    private IEnumerator UpdateTimer()
    {
        while (_timerRunning)
        {
            _elapsedTime += Time.deltaTime;
            _timePlaying = TimeSpan.FromSeconds(_elapsedTime);
            string timePlayingStr = _timePlaying.ToString("mm':'ss':'ff");
            TimeCounter.text = timePlayingStr;

            yield return null;
        }
    }
}
