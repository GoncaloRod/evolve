using System.Timers;
using UnityEngine;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    private bool _dead = false;

    [SerializeField] private int bonesToLife = 7;
    
    public int Life { get; private set; } = 3;
    
    public int Bones { get; private set; } = 0;

    private void Start()
    {
        GameManager.Instance.OnGameOver.AddListener(() =>
        {
            enabled = false;
        });
    }
    
    public void TakeDamage()
    {
        Life--;
        if (Life < 0)
        {
            _dead = true;
            GameManager.Instance.OnGameOver.Invoke();
        }
        else
        {
            GameManager.Instance.OnPlayerDie.Invoke();
        }
            
    }

    public void AddBone()
    {
        Bones++;
        
        if (Bones == bonesToLife)
        {
            AddLife();
            Bones = 0;
        }
    }

    public void AddLife()
    {
        Life++;
    }
    
    
}
