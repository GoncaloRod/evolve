using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public Vector2 MovementInput;
    
    [HideInInspector] public bool IsGroundPounding { get; protected set; }
    
    public virtual void Jump() { }
    
    public virtual void Pound() { }
}
