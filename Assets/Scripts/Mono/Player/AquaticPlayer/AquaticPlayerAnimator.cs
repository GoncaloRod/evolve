using UnityEngine;

[RequireComponent(typeof(AquaticPlayerController))]
public class AquaticPlayerAnimator : MonoBehaviour
{
    private AquaticPlayerController _controller;
    private Animator _animator;
    
    private void Awake()
    {
        _controller = GetComponent<AquaticPlayerController>();
        _animator = GetComponentInChildren<Animator>();
        
        Debug.Assert(_animator != null, $"Missing animator in {name}");
    }

    private void Start()
    {
        _controller.OnPound.AddListener(() => _animator.SetTrigger("Pound"));
    }

    private void Update()
    {
        _animator.SetFloat("Speed", _controller.Speed);
    }
}
