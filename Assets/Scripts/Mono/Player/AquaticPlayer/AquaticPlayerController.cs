using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class AquaticPlayerController : PlayerController
{
    private Rigidbody _rigidbody;

    private Vector3 _dir;

    private float _targetAngle;
    private float _turnSmoothVelocity;

    [Header("Properties")]
    [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private float turnSmoothTime = 0.1f;

    [Header("Ground Pound")]
    [SerializeField] private float groundPoundEffectHeight = 1f;
    [SerializeField] private float groundPoundEffectRadius = 2f;
    
    [Header("Graphics")]
    [SerializeField] private GameObject graphicsObject;
    
    [HideInInspector] public float Speed { get; private set; }

    [HideInInspector] public UnityEvent OnPound;

    private void Awake()
    {
        OnPound = new UnityEvent();
        
        _rigidbody = GetComponent<Rigidbody>();
        
        Debug.Assert(graphicsObject != null, "Missing graphics object reference in {name}");
    }

    private void Update()
    {
        _dir = Vector3.Normalize(
            transform.forward * MovementInput.x +
            transform.up * MovementInput.y
        );
        
        Speed = _dir.magnitude;

        if (MovementInput.x < 0)
            _targetAngle = 180;
        else if (MovementInput.x > 0)
            _targetAngle = 0;

        float angle = Mathf.SmoothDampAngle(
            graphicsObject.transform.eulerAngles.y, _targetAngle, ref _turnSmoothVelocity, turnSmoothTime
        );

        graphicsObject.transform.localRotation = Quaternion.Euler(0f, angle, 0f);
    }

    private void FixedUpdate()
    {
        HandleMovement();
    }

    private void HandleMovement()
    {
        if (_dir == Vector3.zero && _rigidbody.velocity != Vector3.zero)
            _rigidbody.velocity = Vector3.zero;
        
        _rigidbody.MovePosition(transform.position + _dir * (movementSpeed * Time.fixedDeltaTime));
    }

    public override void Pound()
    {
        var others = Physics.OverlapCapsule(
            transform.position + transform.forward * groundPoundEffectHeight / 2f,
            transform.position - transform.forward * groundPoundEffectHeight / 2f,
            groundPoundEffectRadius,
            Int32.MaxValue,
            QueryTriggerInteraction.Ignore
        );
        
        foreach (var other in others)
        {
            var poundable = other.GetComponent<IPoundable>();

            if (poundable != null)
            {
                poundable.OnSmash(gameObject);
            }
        }
        
        OnPound.Invoke();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        
        Gizmos.DrawWireSphere(transform.position + transform.forward * groundPoundEffectHeight / 2f, groundPoundEffectRadius);
        Gizmos.DrawWireSphere(transform.position - transform.forward * groundPoundEffectHeight / 2f, groundPoundEffectRadius);
    }
}
