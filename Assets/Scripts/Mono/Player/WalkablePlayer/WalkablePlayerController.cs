using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CharacterController))]
public class WalkablePlayerController : PlayerController
{
    private CharacterController _controller;
    private Transform _cameraTransform;
    
    private float _turnSmoothVelocity;

    private bool _isGrounded;

    private bool _canJump;
    private bool _canPound;
    
    private int _jumpCounter;

    private float _cameraAngle;

    [Header("Ground Check")]
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundCheckLayer;
    [SerializeField] private float groundCheckRadius = 0.1f;
    
    [Header("Properties")]
    [SerializeField] private float movementSpeed = 1f;
    [SerializeField] private float jumpHeight = 1.2f;
    [SerializeField] private float turnSmoothTime = 0.1f;
    [SerializeField] private bool cameraOrientedMovement = true;
    [SerializeField] private float groundPoundVelocity = 3f;

    [HideInInspector] public float Speed { get; private set; }

    [HideInInspector] public bool Jumping => !_isGrounded;
    
    [HideInInspector] public UnityEvent OnJump { get; private set; }
    
    [HideInInspector] public UnityEvent OnLand { get; private set; }
    
    [HideInInspector] public UnityEvent OnPound { get; private set; }
    
    [HideInInspector] public Vector3 VerticalVelocity;

    private void Awake()
    {
        OnJump = new UnityEvent();
        OnLand = new UnityEvent();
        OnPound = new UnityEvent();
        
        _controller = GetComponent<CharacterController>();
        
        _cameraTransform = Camera.main.transform;
        
        _cameraAngle = _cameraTransform.eulerAngles.y;
    }

    private void Start()
    {
        GameManager.Instance.OnGameOver.AddListener(() =>
        {
            enabled = false;
        });
    }

    private void Update()
    {
        // Create 3D direction vector from user input
        Vector3 dir = new Vector3(MovementInput.x, 0f, MovementInput.y).normalized;

        bool previousIsGrounded = _isGrounded;
        Collider[] others = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundCheckLayer.value, QueryTriggerInteraction.Ignore);
        _isGrounded = others.Length != 0;
        
        if (previousIsGrounded && !_isGrounded)
        {
            TakeOff();
        }
        else if (previousIsGrounded == false && _isGrounded == true)
        {
            Land(others);
        }
        
        _cameraAngle = _cameraTransform.eulerAngles.y;

        Speed = dir.magnitude;
        
        // Only move if direction vector lenght is greater then 0
        // This prevents some math related bugs
        if (dir.magnitude > 0f)
        {
            float targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;

            if (cameraOrientedMovement)
                targetAngle += _cameraAngle;
        
            // Make character smoothly rotate into facing direction 
            float angle = Mathf.SmoothDampAngle(
                transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity, turnSmoothTime
            );
        
            // Apply correct rotation to character
            transform.rotation = Quaternion.Euler(0f, angle, 0.0f);

            if (cameraOrientedMovement)
                dir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            // Apply horizontal movement
            _controller.Move(dir * (movementSpeed * Time.deltaTime));
        }
        
        if (!_isGrounded)
            VerticalVelocity += Physics.gravity * Time.deltaTime;

        // Apply vertical movement
        _controller.Move(VerticalVelocity * Time.deltaTime);
    }

    private void TakeOff()
    {
        _canPound = true;
        _canJump = false;
    }

    private void Land(Collider[] others)
    {
        OnLand.Invoke();
        
        _canJump = true;
        _canPound = false;
        _jumpCounter = 0;
        
        VerticalVelocity = Vector3.zero;

        if (!IsGroundPounding)
            return;
        
        foreach (Collider other in others)
        {
            var poundable = other.GetComponent<IPoundable>();
            poundable?.OnSmash(gameObject);
        }
            
        IsGroundPounding = false;
    }

    public override void Jump()
    {
        if (!_canJump && _jumpCounter >= 2)
            return;
        
        OnJump.Invoke();
        
        VerticalVelocity = transform.up * Mathf.Sqrt(jumpHeight * -2f * Physics.gravity.y);
        _jumpCounter++;
    }

    public override void Pound()
    {
        if (!_canPound)
            return;
        
        OnPound.Invoke();

        VerticalVelocity = Physics.gravity * groundPoundVelocity;
        IsGroundPounding = true;
    }

    private void OnDrawGizmos()
    {
        // Draw ground check
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
