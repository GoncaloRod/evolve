using System;
using UnityEngine;

[RequireComponent(typeof(WalkablePlayerController))]
public class WalkableCharacterAnimator : MonoBehaviour
{
    private WalkablePlayerController _controller;
    private Animator _animator;

    private void Awake()
    {
        _controller = GetComponent<WalkablePlayerController>();
        _animator = GetComponentInChildren<Animator>();
        
        Debug.Assert(_animator != null, $"Missing animator in {name}");
    }

    private void Start()
    {
        _controller.OnJump.AddListener(() => _animator.SetBool("Jumping", true));
        
        _controller.OnLand.AddListener(() =>
        {
            _animator.SetBool("Jumping", false);
            _animator.SetBool("Pounding", false);
        });
        
        _controller.OnPound.AddListener(() => _animator.SetBool("Pounding", true));
    }

    private void Update()
    {
        _animator.SetFloat("Speed", _controller.Speed);
    }
}
