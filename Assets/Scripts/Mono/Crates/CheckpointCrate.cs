using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(MeshRenderer))]
public class CheckpointCrate : MonoBehaviour, IPoundable
{
    private AudioSource _audioSource;

    private Collider _collider;
    private MeshRenderer _meshRenderer;
    
    [SerializeField] private AudioClip breakSound;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

        _collider = GetComponent<Collider>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    public void OnSmash(GameObject player)
    {
        CheckpointManager.Instance.CurrentCheckpoint = transform;
        
        // TODO: Maybe destroy animation
        
        _audioSource.PlayOneShot(breakSound);

        _collider.enabled = false;
        _meshRenderer.enabled = false;
        
        Destroy(gameObject, breakSound.length);
    }
}
