using Unity.Mathematics;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(MeshRenderer))]
public class PointsCrate : MonoBehaviour, IPoundable
{
    private AudioSource _audioSource;

    private Collider _collider;
    private MeshRenderer _meshRenderer;
    
    [SerializeField] private AudioClip breakSound;
    [Space]
    [SerializeField] private GameObject pickupPrefab;

    private void Awake()
    {
        Debug.Assert(pickupPrefab != null, $"Missing pickup prefab in {name}");

        _audioSource = GetComponent<AudioSource>();

        _collider = GetComponent<Collider>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }
    
    public void OnSmash(GameObject player)
    {
        Instantiate(pickupPrefab, transform.position, quaternion.identity);
        
        // TODO: Maybe destroy animation
        
        _audioSource.PlayOneShot(breakSound);

        _collider.enabled = false;
        _meshRenderer.enabled = false;
        
        Destroy(gameObject, breakSound.length);
    }
}
