﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AquaticEnemyController : MonoBehaviour, IPoundable
{
    private Vector3 _startingPosition;

    private int _currentCheckpoint = 0;

    private float _targetAngle;
    
    private float _turnSmoothVelocity;
    
    [Range(0f, 1f)]
    [SerializeField] private float pointSpawnChance = 0.5f;
    [SerializeField] private GameObject pointPickupPrefab;

    [Space]
    [SerializeField] private bool alwaysDealsDamage = false;
    
    [Header("AI")]
    [SerializeField] private Vector3[] checkpoints;

    [Space]
    [SerializeField] private float speed = 1f;
    [SerializeField] private float turnSmoothTime = 0.1f;
    
    private void Awake()
    {
        Debug.Assert(checkpoints.Length > 1, $"{name} needs at least 2 checkpoints");
        Debug.Assert(pointPickupPrefab != null, $"Missing point pickup prefab in {name}");

        _startingPosition = transform.position;
    }

    private void Update()
    {
        Vector3 target = _startingPosition + checkpoints[_currentCheckpoint];
        
        if ((target - transform.position).magnitude <= float.Epsilon)
        {
            _currentCheckpoint = (_currentCheckpoint + 1) % checkpoints.Length;
            target = checkpoints[_currentCheckpoint];
        }

        Vector3 dir = Vector3.Normalize(target - transform.position);
        Vector3 nextPos = transform.position + dir * (speed * Time.deltaTime);
        float maxDistance = (target - transform.position).magnitude;

        transform.position = Vector3.MoveTowards(transform.position, nextPos, maxDistance);

        _targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg - 0f;

        float angle = Mathf.SmoothDampAngle(
            transform.eulerAngles.y, _targetAngle, ref _turnSmoothVelocity, turnSmoothTime
        );

        transform.localRotation = Quaternion.Euler(0f, angle, 0f);
    }

    public void OnSmash(GameObject player)
    {
        if (alwaysDealsDamage)
        {
            var stats = player.GetComponent<PlayerStats>();
            stats.TakeDamage();
        }
        
        Die();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        var stats = other.GetComponent<PlayerStats>();
        
        if (stats == null)
            return;
        
        stats.TakeDamage();
    }
    
    private void Die()
    {
        float rnd = Random.value;

        if (rnd < pointSpawnChance)
            Instantiate(pointPickupPrefab, transform.position, Quaternion.identity);
        
        // TODO: Animation
        // TODO: Play cool sound
        // TODO: Maybe some particles
        
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        if (checkpoints == null)
            return;

        Vector3 start = Application.isPlaying ? _startingPosition : transform.position;
        
        Gizmos.color = Color.red;
        
        for (int i = 0; i < checkpoints.Length; i++)
        {
            Gizmos.DrawSphere(start + checkpoints[i], 0.1f);
            Gizmos.DrawLine(start + checkpoints[i], start + checkpoints[(i + 1) % checkpoints.Length]);
        }
    }
}
