using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public class WalkableEnemyController : MonoBehaviour
{
    private NavMeshAgent _agent;

    private int _currentCheckpoint = 0;
    
    [Range(0f, 1f)]
    [SerializeField] private float pointSpawnChance = 0.5f;
    [SerializeField] private GameObject pointPickupPrefab;
    
    [Space]
    [SerializeField] private bool alwaysDealsDamage = false;

    [Header("AI")]
    public Vector3[] patrolCheckpoints;

    [HideInInspector] public float Speed => _agent.velocity.magnitude;

    private void Awake()
    {
        Debug.Assert(pointPickupPrefab != null, $"Missing point pickup prefab in {name}");

        _agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        if (patrolCheckpoints.Length == 0)
            return;
        
        _agent.SetDestination(patrolCheckpoints[_currentCheckpoint]);
    }

    private void Update()
    {
        if (patrolCheckpoints.Length == 0)
            return;
        
        if (Vector3.Distance(transform.position, patrolCheckpoints[_currentCheckpoint]) <= _agent.stoppingDistance)
        {
            _currentCheckpoint = (_currentCheckpoint + 1) % patrolCheckpoints.Length;
            _agent.SetDestination(patrolCheckpoints[_currentCheckpoint]);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var stats = other.GetComponent<PlayerStats>();
        
        if (stats == null)
            return;
        
        var motor = other.GetComponent<PlayerController>();
        
        if (motor == null)
            return;

        if (motor.IsGroundPounding)
        {
            if (alwaysDealsDamage)
            {
                stats.TakeDamage();
            }
            
            Die();
        }
        else
        {
            stats.TakeDamage();
        }
    }

    private void Die()
    {
        float rnd = Random.value;

        if (rnd < pointSpawnChance)
            Instantiate(pointPickupPrefab, transform.position, Quaternion.identity);
        
        // TODO: Animation
        // TODO: Play cool sound
        // TODO: Maybe some particles
        
        Destroy(gameObject);
    }
}
