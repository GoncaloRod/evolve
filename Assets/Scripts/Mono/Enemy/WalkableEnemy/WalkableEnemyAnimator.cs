using UnityEngine;

[RequireComponent(typeof(WalkableEnemyController))]
public class WalkableEnemyAnimator : MonoBehaviour
{
    private WalkableEnemyController _controller;
    private Animator[] _animators;

    private void Awake()
    {
        _controller = GetComponent<WalkableEnemyController>();
        _animators = GetComponentsInChildren<Animator>();
        
        Debug.Assert(_animators.Length != 0, $"Missing animator in {name}");
    }

    private void Update()
    {
        foreach (var animator in _animators)
        {
            animator.SetFloat("Speed", _controller.Speed);            
        }
    }
}
