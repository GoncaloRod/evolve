using UnityEngine;

public interface IPoundable
{
    public void OnSmash(GameObject player);
}
