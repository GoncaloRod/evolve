#if UNITY_EDITOR
using UnityEditor;

public class TextureProcessor : AssetPostprocessor
{
    private void OnPreprocessTexture()
    {
        TextureImporter importer = assetImporter as TextureImporter;
        
        if (importer == null) return;

        // Make sure this only runs on the first import
        if (importer.importSettingsMissing)
        {
            if (importer.assetPath.ToUpper().Contains("NORMAL"))
            {
                importer.textureType = TextureImporterType.NormalMap;
            }
        }
    }
}

#endif
