#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class ModelProcessor : AssetPostprocessor
{
    private void OnPreprocessModel()
    {
        ModelImporter importer = assetImporter as ModelImporter;

        if (importer == null) return;

        // Make sure this only runs on the first import
        if (importer.importSettingsMissing)
        {
            importer.materialImportMode = ModelImporterMaterialImportMode.None;
        }
    }

    private void OnPostprocessModel(GameObject g)
    {
        g.transform.rotation = Quaternion.identity;
    }
}

#endif
