#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WalkableEnemyController))]
public class EnemyControllerEditor : Editor
{
    private WalkableEnemyController _controller;

    private void OnEnable()
    {
        _controller = (WalkableEnemyController) target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }

    private void OnSceneGUI()
    {
        Vector3[] checkpoints = _controller.patrolCheckpoints;
        
        for (int i = 0; i < checkpoints.Length; i++)
        {
            Handles.color = Color.red;
            
            Handles.Label(checkpoints[i], $"{i}");
            
            Handles.DrawLine(checkpoints[i], checkpoints[(i + 1) % checkpoints.Length]);
            
            //checkpoints[i] = Handles.PositionHandle(checkpoints[i], Quaternion.identity);
        }
    }
}
#endif
